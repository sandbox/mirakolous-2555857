var baseUrl = drupalSettings.reveal_js.reveal_js.baseUrl;      
Reveal.initialize({
  controls: drupalSettings.reveal_js.reveal_js.controls,
  progress: drupalSettings.reveal_js.reveal_js.progress,
  slideNumber: drupalSettings.reveal_js.reveal_js.slideNumber,
  history: drupalSettings.reveal_js.reveal_js.history,
  keyboard: drupalSettings.reveal_js.reveal_js.keyboard,
  overview: drupalSettings.reveal_js.reveal_js.overview,
  center: drupalSettings.reveal_js.reveal_js.center,
  touch: drupalSettings.reveal_js.reveal_js.touch,
  loop: drupalSettings.reveal_js.reveal_js.loop,
  rtl: drupalSettings.reveal_js.reveal_js.rtl,
  fragments: drupalSettings.reveal_js.reveal_js.fragments,
  embedded: drupalSettings.reveal_js.reveal_js.embedded,
  help: drupalSettings.reveal_js.reveal_js.help,
  showNotes: drupalSettings.reveal_js.reveal_js.showNotes,
  // autoSlide: drupalSettings.reveal_js.reveal_js.autoSlide,
  autoSlideStoppable: drupalSettings.reveal_js.reveal_js.autoSlideStoppable,
  mouseWheel: drupalSettings.reveal_js.reveal_js.mouseWheel,
  hideAddressBar: drupalSettings.reveal_js.reveal_js.hideAddressBar,
  previewLinks: drupalSettings.reveal_js.reveal_js.previewLinks,
  transitionSpeed: drupalSettings.reveal_js.reveal_js.transitionSpeed,
  backgroundTransition: drupalSettings.reveal_js.reveal_js.backgroundTransition,
  viewDistance: drupalSettings.reveal_js.reveal_js.viewDistance,
  theme: Reveal.getQueryHash().theme, // available themes are in /css/theme
  transition: Reveal.getQueryHash().transition || drupalSettings.reveal_js.reveal_js.transition, // default/cube/page/concave/zoom/linear/fade/none
  // Optional libraries used to extend on reveal.js
  theme: Reveal.getQueryHash().theme, // available themes are in /css/theme
  transition: Reveal.getQueryHash().transition || 'default', // default/cube/page/concave/zoom/linear/fade/none
  // Optional libraries used to extend on reveal.js
  dependencies: [
    { src: baseUrl + '/modules/reveal_js/reveal_js/lib/js/classList.js', condition: function() { return !document.body.classList; } },
    { src: baseUrl + '/modules/reveal_js/reveal_js/plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
    { src: baseUrl + '/modules/reveal_js/reveal_js/plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
    { src: baseUrl + '/modules/reveal_js/reveal_js/plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },
    { src: baseUrl + '/modules/reveal_js/reveal_js/plugin/zoom-js/zoom.js', async: true, condition: function() { return !!document.body.classList; } },
    { src: baseUrl + '/modules/reveal_js/reveal_js/plugin/notes/notes.js', async: true, condition: function() { return !!document.body.classList; } }
  ]
});