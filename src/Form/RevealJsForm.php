<?php

/**
* @file
* Contains \Drupal\reveal_js\Form\RevealJsForm
*/

namespace Drupal\reveal_js\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class RevealJsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['reveal_js.settings'];
  }

  /**
  * {@inheridoc}
  */
  public function getFormId() {
    return 'reveal_js_form';
  }
  
  /**
  * {@inheridoc}
  */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('reveal_js.settings');

    $form['reveal_js_controls'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Controls'),
      '#description' => $this->t('Display controls in the bottom right corner'),
      '#default_value' => $config->get('controls'),
    );

    $form['reveal_js_progress'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Progress'),
      '#description' => $this->t('Displays a presentation progress bar'),
      '#default_value' => $config->get('progress'),
    );

    $form['reveal_js_slideNumber'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Slide Number'),
      '#description' => $this->t('Displays the page number of the current slide'),
      '#default_value' => $config->get('slideNumber'),
    );

    $form['reveal_js_history'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('History'),
      '#description' => $this->t('Push each slide change to the browser history'),
      '#default_value' => $config->get('history'),
    );

    $form['reveal_js_keyboard'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Keyboard'),
      '#description' => $this->t('Enable keyboard shortcuts for navigation'),
      '#default_value' => $config->get('keyboard'),
    );

    $form['reveal_js_overview'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Overview'),
      '#description' => $this->t('Enable the slide overview mode'),
      '#default_value' => $config->get('overview'),
    );

    $form['reveal_js_center'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Center'),
      '#description' => $this->t('Vertical centering of slides'),
      '#default_value' => $config->get('center'),
    );

    $form['reveal_js_touch'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Touch'),
      '#description' => $this->t('Enables touch navigation on devices with touch input'),
      '#default_value' => $config->get('touch'),
    );

    $form['reveal_js_loop'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Loop'),
      '#description' => $this->t('Loop the presentation'),
      '#default_value' => $config->get('loop'),
    );

    $form['reveal_js_rtl'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('RTL'),
      '#description' => $this->t('Change the presentation direction to be RTL'),
      '#default_value' => $config->get('rtl'),
    );

    $form['reveal_js_fragments'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Fragments'),
      '#description' => $this->t('Turns fragments on and off globally'),
      '#default_value' => $config->get('fragments'),
    );

    $form['reveal_js_embedded'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Embedded'),
      '#description' => $this->t('Flags if the presentation is running in an embedded mode, i.e. contained within a limited portion of the screen'),
      '#default_value' => $config->get('embedded'),
    );

    $form['reveal_js_help'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Help'),
      '#description' => $this->t('Flags if we should show a help overlay when the questionmark key is pressed'),
      '#default_value' => $config->get('help'),
    );

    $form['reveal_js_showNotes'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Show Notes'),
      '#description' => $this->t('Flags if speaker notes should be visible to all viewers'),
      '#default_value' => $config->get('showNotes'),
    );

    $form['reveal_js_autoSlide'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Auto Slide'),
      '#description' => $this->t('Number of milliseconds between automatically proceeding to the next slide, disabled when set to 0, this value can be overwritten by using a data-autoslide attribute on your slides'),
      '#default_value' => $config->get('autoSlide'),
    );

    $form['reveal_js_autoSlideStoppable'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Auto Slide Stoppable'),
      '#description' => $this->t('Stop auto-sliding after user input'),
      '#default_value' => $config->get('autoSlideStoppable'),
    );

    $form['reveal_js_mouseWheel'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Mouse Wheel'),
      '#description' => $this->t('Enable slide navigation via mouse wheel'),
      '#default_value' => $config->get('mouseWheel'),
    );

    $form['reveal_js_hideAddressBar'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Hide Address Bar'),
      '#description' => $this->t('Hides the address bar on mobile devices'),
      '#default_value' => $config->get('hideAddressBar'),
    );

    $form['reveal_js_previewLinks'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Preview Links'),
      '#description' => $this->t('Opens links in an iframe preview overlay'),
      '#default_value' => $config->get('previewLinks'),
    );

    $form['reveal_js_transition'] = array(
      '#type' => 'select',
      '#title' => $this->t('Transition'),
      '#options' => array(
        'default' => t('Default'),
        'none'  => t('None'),
        'fade' => t('Fade'),
        'slide' => t('Slide'),
        'convex' => t('Convex'),
        'concave' => t('Concave'),
        'zoom' => t('Zoom'),
      ),
      '#description' => $this->t('Transition style'),
      '#default_value' => $config->get('transition'),
    );

    $form['reveal_js_transitionSpeed'] = array(
      '#type' => 'select',
      '#title' => $this->t('Transition Speed'),
      '#options' => array(
        'default' => t('Default'),
        'fast'  => t('Fast'),
        'fade' => t('Fade'),
        'slow' => t('Slow'),
      ),
      '#description' => $this->t('Transition speed'),
      '#default_value' => $config->get('transitionSpeed'),
    );

    $form['reveal_js_backgroundTransition'] = array(
      '#type' => 'select',
      '#title' => $this->t('Background Transition'),
      '#options' => array(
        'default' => t('Default'),
        'none'  => t('None'),
        'fade' => t('Fade'),
        'slide' => t('Slide'),
        'convex' => t('Convex'),
        'concave' => t('Concave'),
        'zoom' => t('Zoom'),
      ),
      '#description' => $this->t('Transition style for full page slide backgrounds'),
      '#default_value' => $config->get('backgroundTransition'),
    );

    $form['reveal_js_viewDistance'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('View Distance'),
      '#description' => $this->t('Number of slides away from the current that are visible'),
      '#default_value' => $config->get('viewDistance'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
  * {@inheritdoc}
  */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Set variables based on form values
    $controls = $form_state->getValue('reveal_js_controls');
    $progress = $form_state->getValue('reveal_js_progress');
    $slideNumber = $form_state->getValue('reveal_js_slideNumber');
    $history = $form_state->getValue('reveal_js_history');
    $keyboard = $form_state->getValue('reveal_js_keyboard');
    $overview = $form_state->getValue('reveal_js_overview');
    $center = $form_state->getValue('reveal_js_center');
    $touch = $form_state->getValue('reveal_js_touch');
    $loop = $form_state->getValue('reveal_js_loop');
    $rtl = $form_state->getValue('reveal_js_rtl');
    $fragments = $form_state->getValue('reveal_js_fragments');
    $embedded = $form_state->getValue('reveal_js_embedded');
    $help = $form_state->getValue('reveal_js_help');
    $showNotes = $form_state->getValue('reveal_js_showNotes');
    $autoSlide = $form_state->getValue('reveal_js_autoSlide');
    $autoSlideStoppable = $form_state->getValue('reveal_js_autoSlideStoppable');
    $mouseWheel = $form_state->getValue('reveal_js_mouseWheel');
    $hideAddressBar = $form_state->getValue('reveal_js_hideAddressBar');
    $previewLinks = $form_state->getValue('reveal_js_previewLinks');
    $transition = $form_state->getValue('reveal_js_transition');
    $transitionSpeed = $form_state->getValue('reveal_js_transitionSpeed');
    $backgroundTransition = $form_state->getValue('reveal_js_backgroundTransition');
    $viewDistance = $form_state->getValue('reveal_js_viewDistance');

    // Get the config object.
    $config = \Drupal::service('config.factory')->getEditable('reveal_js.settings');

    // Set the values the user submitted in the form
    $config->set('label', $label);

    $config->set('controls', $controls);
    $config->set('progress', $progress);
    $config->set('slideNumber', $slideNumber);
    $config->set('history', $history);
    $config->set('keyboard', $keyboard);
    $config->set('overview', $overview);
    $config->set('center', $center);
    $config->set('touch', $touch);
    $config->set('loop', $loop);
    $config->set('rtl', $rtl);
    $config->set('fragments', $fragments);
    $config->set('embedded', $embedded);
    $config->set('help', $help);
    $config->set('showNotes', $showNotes);
    $config->set('autoSlide', $autoSlide);
    $config->set('autoSlideStoppable', $autoSlideStoppable);
    $config->set('mouseWheel', $mouseWheel);
    $config->set('hideAddressBar', $hideAddressBar);
    $config->set('previewLinks', $previewLinks);
    $config->set('transition', $transition);
    $config->set('transitionSpeed', $transitionSpeed);
    $config->set('backgroundTransition', $backgroundTransition);
    $config->set('viewDistance', $viewDistance);
    $config->save();
  }
}